import React from "react";
import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav>
      <ul>
        <li>
          <NavLink to="/" activeClassName="active">
            Version 1
        </NavLink>
        </li>
        <NavLink to="/v2" activeClassName="active">
          <li> Version 2</li>
        </NavLink>
      </ul>
    </nav>
  );
}

export default Nav;
