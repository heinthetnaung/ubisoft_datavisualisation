import React from "react";

function Searchbar() {
  return (
    <div id="control-panel" className="card clearfix">
      <div>
        <label>
          <span className="ti-calendar"></span> Date:
        </label>
        <input type="text" id="monitorDate" size="20" defaultValue="2018-10-05" className="form-control d-inline" />
      </div>
      <div>
        <label>
          <span className="ti-user"></span> Players:{" "}
        </label>
        <select id="player" className="form-control  d-inline">
          <option defaultValue="">All Players</option>
        </select>
        <span id="player-value"></span>
      </div>
      <hr />
      <div id="todayInfo">
        <label className="player_density">
          Avg Player Density: <span id="total_player"> 0 </span>{" "}
        </label>
        <label className="AI_density text-right">
          Avg AI Density: <span id="total_ai"> 0 </span>
        </label>
      </div>

      <div id="tooltip"></div>
    </div>
  );
}

export default Searchbar;
