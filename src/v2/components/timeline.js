import React from "react";

function Timeline() {
    return (
    <div id="timeline">
            <div id="rngOutput" className="text-center"> TIME </div>
            <div className="playBtnWrapper"><a title="Play video" className="play" id="play"></a></div>
            <div className="rangeWrapper">  <input type="range" id="rng" min="0" defaultValue="0" /> </div>
    </div>
    );
}

export default Timeline;





