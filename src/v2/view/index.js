import React, {Component} from "react";

import '../style/v2/style.css';
import TIMELINE from "../components/timeline";
import SEARCHBAR from "../components/searchbar";

class App extends Component {
  componentDidMount() {
    window.initv2();
  }

  render = () => {
    return (
      <div id="v2">
        <div id="logo">
          <img src="./assets/image/ubi_logo.webp" alt="logo" />
        </div>
        <SEARCHBAR />
        <TIMELINE />
      </div>
    );
  }
}

export default App;
