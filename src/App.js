import React from 'react';

import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';

import V1VIEW from "./v1/view";
import V2VIEW from "./v2/view";
import NAV from "./nav";

function App() {
  return (
    <Router>
      <div className="ubisoft">
        <NAV />
        <Switch>
          <Route path="/" exact component={V1VIEW} />
          <Route path="/v2" component={V2VIEW} />
        </Switch>
      </div>
    </Router>
  );
}

export default App;
