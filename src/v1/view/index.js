import React, { Component } from 'react';

import '../style/v1/style.css';

import TIMEPICKER from '../components/timepicker';
import PLAYERMONITOR from '../components/bubblechart';
import RECORDBOX from '../components/recordbox';
import HISTORYBARCHART from '../components/barchart';
import HEADER from '../components/header';


class App extends Component {

  componentDidMount() {
    window.initv1();
  }

  render = () => {
    return (
      <div id="v1">
        <HEADER />
        <div className="container">
          <RECORDBOX />
          <div className="row">
            <div className="col-12">
              <TIMEPICKER />
              <PLAYERMONITOR />
            </div>
          </div>
          <div className="row">
            <HISTORYBARCHART />
          </div>
        </div>
      </div>
    );
  }
}

export default App;
