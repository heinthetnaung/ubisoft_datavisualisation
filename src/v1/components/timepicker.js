import React , { Component } from 'react';

class App extends Component{

componentDidMount(){
    var input = document.getElementById('monitorDate');
    
    var picker = window.TheDatepicker;
    var datepicker = new picker.Datepicker(input);
    datepicker.options.setInputFormat("Y-m-d");
    datepicker.options.onSelect((event, day, previousDay) => {
      var date = document.getElementById('monitorDate');
  
      window.bubblechart.destroy();
      if(date.value !== ""){
        window.renderLiveMonitor(date.value, window.mainData);
      }else{
        window.renderLiveMonitor("2018-09-25", window.mainData);
      }
  
    });
  
    datepicker.render();
}

    render(){
        return(
            <div className="form-group">
            <label htmlFor="monitorDate">Date:</label>
            <div id="input-wrapper">
              <input
                type="text"
                id="monitorDate"
                size="20"
                placeholder="2018-09-25"
                className="form-control"
                ref="timepicker"
              />
            </div>
          </div>   
        )
    }

}

export default App