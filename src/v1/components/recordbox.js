import React , { Component } from 'react';

class App extends Component{

    render(){
        return(
            <div className="row">
            <div className="col-md-3 col-sm-4 col-6 mb-3">
              <div className="card">
                <div className="card-header">
                  Total Players
                </div>
                <div className="card-body text-center">
                  <h2 className="card-title" id="totalPlayers">No Records</h2>
                </div>
              </div>
            </div>
  
            <div className="col-md-3 col-sm-4 col-6 mb-3" id="onlinePlayer">
              <div className="card">
                <div className="card-header">
                  Online Players
                </div>
                <div className="card-body text-center">
                  <h2 className="card-title" id="onlinePlayers">
                    No Records
                  </h2>
                </div>
              </div>
            </div>
          </div>
        )
    }

}

export default App