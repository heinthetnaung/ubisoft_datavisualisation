var mainData;

// INITIALIZE CSV FILE
function initv1() {

  if (document.getElementById("deckgl-overlay")) {
    document.getElementById("deckgl-overlay").classList.add("inactive");
  }

  d3.csv("./data/density_dataset.csv").then(function (data) {
    const csvData = data.filter(function (d) {
      if (d["player"] != "") {
        return d;
      }
    });
    mainData = csvData;
    setTotalPlayer(csvData);
    renderLiveMonitor("2018-09-25", csvData);  //It should be today date();
    yearChart("2018", csvData);
  });
}

// GET UNIQUE PLAYER FROM CSV file
function uniquePlayer(data) {
  mainData = data;
  //Sort EventTime
  data.sort(function (a1, b1) {
    return new Date(b1.event_time) - new Date(a1.event_time);
  });

  //D3 Mapping for player filters.
  var playerList = d3.map();
  data.forEach(function (d) {
    if (!playerList.has(d.player)) {
      playerList.set(d.player, [d]);
    } else {
      playerList.get(d.player).push(d);
    }
  });
  return playerList;
}

//#Set Data
//SET TOTAL PLAYER
function setTotalPlayer(data) {
  document.getElementById("totalPlayers").innerHTML = uniquePlayer(data).size();
}

// Live Motnitor chart.JS
var playerData_aiDensity,
  playerDate_noDeisity,
  playerData_playerDensity,
  playerDate_bothDensity,
  todayPlayer;

function playerMonitor(date, data) {
  playerData_aiDensity = [],
    playerDate_noDeisity = [],
    playerData_playerDensity = [],
    playerDate_bothDensity = [];
  todayPlayer = 0;
  var tempPlayer = [];

  const filterPlayer = data.filter(function (d) {

    if (d["event_time"].includes(date)) {

      if (!tempPlayer.includes(d["player"])) {
        tempPlayer += d["player"];
        todayPlayer += 1;
      }
      if (Number(d["ai_density"]) > 0 && Number(d["player_density"] == 0)) {
        playerData_aiDensity.push({
          x: "" + d["xcoordinate"],
          y: "" + d["ycoordinate"],
          r: "" + (Number(d["ai_density"]) * 2),
        });
      } else if (Number(d["ai_density"]) == 0 && Number(d["player_density"] > 0)) {
        playerData_playerDensity.push({
          x: "" + d["xcoordinate"],
          y: "" + d["ycoordinate"],
          r: "" + (Number(d["player_density"]) * 2),
        });
      } else if (Number(d["ai_density"]) > 0 && Number(d["player_density"] > 0)) {
        playerDate_bothDensity.push({
          x: "" + d["xcoordinate"],
          y: "" + d["ycoordinate"],
          r: "" + ((Number(d["player_density"]) + Number(d["ai_density"])) * 2),
        });
      } else {
        playerDate_noDeisity.push({
          x: "" + d["xcoordinate"],
          y: "" + d["ycoordinate"],
          r: "" + 1,
        });
      }
    }
  });
}

var bubblechart;
function renderLiveMonitor(date, data) {
  playerMonitor(date, data);
  var data = {
    datasets: [
      {
        label: 'AI Density',
        data: playerData_aiDensity,
        hoverRadius: false,
        backgroundColor: "rgba(255, 212, 0, 0.4)",
      },
      {
        label: 'Empty Density',
        data: playerDate_noDeisity,
        hoverRadius: false,
        backgroundColor: "rgba(51, 208, 0, 0.4)",
      },
      {
        label: 'Player Density',
        data: playerData_playerDensity,
        hoverRadius: false,
        backgroundColor: "rgba(0, 173, 255, 0.4)",
      },
      {
        label: 'AI Density + Player Density',
        data: playerDate_bothDensity,
        hoverRadius: false,
        backgroundColor: "rgba(255, 99, 132, 0.4)",
      },

    ],
  };

  bubblechart = new Chart("monitorCanvas", {
    type: "bubble",
    data: data,
    options: {
      responsive: true,
      tooltips: false,
      title: {
        display: true,
        text: `(Total Players - ${todayPlayer}) Map Density within a day`,
      },
      scales: {
        xAxes: [
          {
            position: "bottom",
            gridLines: {
              zeroLineColor: "rgba(0,0,0,1)",
            },
            ticks: { min: -16384, max: 16384 },
          },
        ],
        yAxes: [
          {
            type: "linear", // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
            display: true,
            position: "left",
            id: "y-axis-1",
            ticks: { min: -16384, max: 16384 },
          },
        ],
      },
      pan: {
        enabled: true,
        mode: "xy",
        rangeMin: {
          // Format of min pan range depends on scale type
          x: -16384,
          y: -16384,
        },
        rangeMax: {
          // Format of max pan range depends on scale type
          x: 16384,
          y: 16384,
        },
      },
      zoom: {
        enabled: true,
        mode: "xy",
        rangeMin: {
          // Format of min pan range depends on scale type
          x: -16384,
          y: -16384,
        },
        rangeMax: {
          // Format of max pan range depends on scale type
          x: 16384,
          y: 16384,
        },
      },
    },
  });
  bubblechart.update();
}

var playerDensity = new Array(12).fill(0),
  aiDensity = new Array(12).fill(0),
  player = new Array(12).fill(0),
  tempPlayer = new Array(12).fill([]);

function filterRecords(year, data) {
  data.filter(function (d) {
    if (d["event_time"].includes(year)) {
      var dateParts = d["event_time"].split("-");
      playerDensity[(Number(dateParts[1] - 1))] += Number(d["player_density"]);
      aiDensity[(Number(dateParts[1] - 1))] += Number(d["ai_density"]);

      if (!tempPlayer[(Number(dateParts[1] - 1))].includes(d["player"])) {
        tempPlayer[(Number(dateParts[1] - 1))] += d["player"];
        player[(Number(dateParts[1] - 1))] += 1;
      }
    }
  });

  aiDensity.forEach(function (data, index) {
    aiDensity[index] = Math.ceil(data / player[index]);
    playerDensity[index] = Math.ceil(playerDensity[index] / player[index]);
  });
}

function yearChart(year, data) {
  filterRecords(year, data);
  // Year Bar Chart
  var barChartData = {
    labels: [
      "January",
      "February",
      "March",
      "April",
      "May",
      "June",
      "July",
      "August",
      "September",
      "October",
      "November",
      "December",
    ],
    datasets: [
      {
        label: "Total Players",
        backgroundColor: window.chartColors.red,
        yAxisID: "y-axis-2",
        data: player,
      },
      {
        label: "Average AI Density",
        backgroundColor: window.chartColors.grey,
        yAxisID: "y-axis-2",
        data: aiDensity,
      },
      {
        label: "Average Player Density",
        backgroundColor: window.chartColors.yellow,
        yAxisID: "y-axis-2",
        data: playerDensity,
      },
    ],
  };

  var ctx = document.getElementById("yearCanvas").getContext("2d");
  window.myBar = new Chart(ctx, {
    type: "bar",
    data: barChartData,
    options: {
      responsive: true,
      title: {
        display: true,
        text: `Yearly Report (Year - ${year})`,
      },
      tooltips: {
        mode: "index",
        intersect: true,
      },
      scales: {
        yAxes: [
          {
            type: "linear", // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
            display: true,
            position: "left",
            id: "y-axis-2",
            gridLines: {
              drawOnChartArea: false,
            },
          },
        ],
      },
    },
  });
}

function debug() {
  console.log("debug");
}
