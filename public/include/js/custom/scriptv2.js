var playerList,
  gl_maindata,
  gl_playdate = [],
  gl_playerTime = [],
  gl_startPlay,
  coordinate,
  playerv2 = "",
  myRange,
  playerCoordinateTime = [];

var rng,
    ro,
    DeckGL,
    OPTIONS,
    COLOR_RANGE;


// INITIALIZE CSV FILE
function initv2() {

  rng = document.getElementById("rng");
  ro = document.getElementById("rngOutput");

  //Design Fix
  document.getElementsByTagName("BODY")[0].classList.add("fullScreen");
  document.getElementsByTagName("nav")[0].classList.add("d-none");

  d3.csv("./data/density_dataset.csv").then(function (data) {
    const csvDatav2 = data.filter(function (d) {
      if (d["player"] != "") {
        return d;
      }
    });

    gl_maindata = csvDatav2;

    getPlaydate(gl_maindata);
    playerMonitorV2("2018-10-05", csvDatav2, playerv2);
    renderLayer(coordinate);
    loadPlugins(gl_playdate);
  });

  // Initialize DeckGL
  DeckGL = new deck.DeckGL({
    longitude: -0.3,
    latitude: 0.7,
    zoom: 8,
    boxZoom: false,
    minZoom: 8,
    pitch: 40.5,
    transitionDuration: {
      elevationScale: 3000,
    },
  });

  OPTIONS = ["radius"];
  COLOR_RANGE = [
    [1, 152, 189],
    [73, 227, 206],
    [216, 254, 181],
    [254, 237, 177],
    [254, 173, 84],
    [209, 55, 78],
  ];
}

function getPlaydate(data) {
  const filterPlayerDate = data.filter(function (d) {
    var temp_date = d["event_time"];
    temp_date = temp_date.split(" ")[0];

    if (!gl_playdate.includes(temp_date)) {
      gl_playdate.push(temp_date);
    }
  });
}

function playerMonitorV2(date, data, playerv2) {
  coordinate = [];

  var tempPlayer = [];
  var AI_density = 0,
    player_density = 0;

  const filterPlayer = data.filter(function (d) {
    if (playerv2 == "") {
      if (d["event_time"].includes(date)) {
        var arr_long_lat = long_lat(d["xcoordinate"], d["ycoordinate"]);
        AI_density += Number(d["ai_density"]);
        player_density += Number(d["player_density"]);
        coordinate.push({
          ai_density: d["ai_density"],
          player_density: d["player_density"],
          PlayerID: d["player"],
          Date: d["event_time"],
          COORDINATE: arr_long_lat,
        });
        if (!tempPlayer.includes(d["player"])) {
          tempPlayer.push(d["player"]);
        }
      }
    } else if (d["event_time"].includes(date) && d["player"].includes(playerv2)) {
      tempPlayer.push(playerv2);
      var arr_long_lat = long_lat(d["xcoordinate"], d["ycoordinate"]);
      AI_density += Number(d["ai_density"]);
      player_density += Number(d["player_density"]);
      coordinate.push({
        ai_density: d["ai_density"],
        player_density: d["player_density"],
        PlayerID: d["player"],
        Date: d["event_time"],
        COORDINATE: arr_long_lat,
      });
    }
  });

  document.getElementById("total_ai").innerHTML = parseInt(AI_density / tempPlayer.length);
  document.getElementById("total_player").innerHTML = parseInt(player_density / tempPlayer.length);

  if (playerv2 == "") {
    var playerSelectInput = document.getElementById("player");
    playerSelectInput.options.length = 1;

    tempPlayer.forEach(function (element) {
      var option = document.createElement("option");
      option.text = element;
      option.value = element;
      playerSelectInput.add(option);
    });
  }
}

function long_lat(a, b) {
  var R = 16384;
  var x = a;
  var y = b;
  var long = Math.atan2(y, x).toFixed(6);
  var lat = Math.acos(y / (R * Math.sin(long))).toFixed(6);

  return {
    lng: long,
    lat: lat,
  };
}

function updateTooltip({ x, y, object }) {
  const tooltip = document.getElementById("tooltip");
  if (object) {
    tooltip.style.top = `${y}px`;
    tooltip.style.left = `${x}px`;
    tooltip.innerHTML = `<p style="padding: 15px;"> x: ${object.position[0]}, y: ${object.position[1]}, density: ${object.points[0].density} </p>`;
  } else {
    tooltip.innerHTML = "";
  }
}

function renderLayer(csvDatav2) {
  const options = { radius: 1 };
  const data = csvDatav2;

  const heatmap = new deck.HeatmapLayer({
    id: "heatmp-layer",
    colorRange: COLOR_RANGE,
    data,
    intensity: 1,
    threshold: 0.03,
    radiusPixels: 30,
    opacity: 0.3,
    pickable: true,
    getPosition: d => [Number(d["COORDINATE"].lng), Number(d["COORDINATE"].lat)],
    getSourcePosition: d => Number(d.lng), // popup
    getTargetPosition: d => Number(d.lat), // popup
    onHover: updateTooltip,
    ...options,
  });

  const bitmaplayer = new deck.BitmapLayer({
    id: "bitmap-layer",
    bounds: [
      [-2.033, -0.076], //left botom
      [-2.033, 3.076], // left top
      [2.033, 3.076], //right top
      [2.033, -0.076], // right bottom
    ],
    opacity: 0.5,
    image: "./assets/image/map.jpg",
  });

  DeckGL.setProps({
    layers: [bitmaplayer, heatmap],
  });
}

function loadPlugins(playerDate) {
  var foopicker = new FooPicker({
    id: "monitorDate",
    dateFormat: "yyyy-MM-dd",
    disable: playerDate,
  });

  var foopickerEvent = foopicker.selectDate;

  foopicker.selectDate = function (event) {
    foopickerEvent(event);
    playerv2 = "";
    playerMonitorV2(foopicker.selectedDate, gl_maindata, playerv2);
    renderLayer(coordinate);
    var timeline = document.getElementById("timeline");
    timeline.classList.remove("active");
  };

  document.addEventListener("contextmenu", event => event.preventDefault());

  const playButton = document.getElementById("play");
  playButton.addEventListener("click", function (e) {

    e.preventDefault();

    if (Number(rng.value) >= Number(rng.max)) {
      rng.value = 0;
    }

    if (playButton.classList.contains("active")) {
      clearInterval(gl_startPlay);
      playButton.classList.remove("active");
    } else {
      playButton.classList.add("active");
      gl_startPlay = setInterval(function () {
        if (Number(rng.value) >= Number(rng.max)) {
          clearInterval(gl_startPlay);
          playButton.classList.remove("active");
        } else {
          rng.value = Number(rng.value) + 1;
          updateRange();
        }
      }, 300);
    }
  });

  const selectPlayer = document.getElementById("player");
  selectPlayer.addEventListener("change", function () {
    getPlayer();
  });
}

function getPlayer() {
  var e = document.getElementById("player");
  var playerID = e.options[e.selectedIndex].value;
  var date = document.getElementById("monitorDate");

  var timeline = document.getElementById("timeline");
  if (date.value !== "") {
    playerMonitorV2(date.value, gl_maindata, playerID);
    renderLayer(coordinate);
    getPlayTime(coordinate);
  }

  if (playerID !== "") {
    timeline.classList.add("active");
  } else {
    timeline.classList.remove("active");
  }
}

function getPlayTime(csvDatav2) {
  var playerTime = [];
  const filterPlayerDate = csvDatav2.filter(function (d) {
    var temp_time = d["Date"];
    temp_time = temp_time.split(" ")[1];

    rng.value = 0; //Reset Timeline to 0;

    if (!playerTime.includes(temp_time)) {
      playerTime.push(temp_time);
    }
  });

  playerCoordinateTime = []; // Reset playerCoordinateTime
  myRange = playerTime;
  rng.max = playerTime.length - 1;

  var i = 0;

  ro.textContent = myRange[parseInt(rng.value, 10)];
  playerCoordinateTime.push(csvDatav2[i]);
  while (i <= rng.max) {
    playerCoordinateTime.push(csvDatav2[i]);
    i++;
  }

  renderLayer(playerCoordinateTime);

  window.addEventListener("DOMContentLoaded", updateRange);
  rng.addEventListener("input", updateRange);
}

function updateRange() {
  ro.textContent = myRange[parseInt(rng.value, 10)];
  var tempPlayerTimeline = [];

  var i = 0;
  while (i <= rng.value) {
    tempPlayerTimeline.push(playerCoordinateTime[i]);
    i++;
  }

  renderLayer(tempPlayerTimeline);
}
